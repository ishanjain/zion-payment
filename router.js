const { PAYTM_MID, PAYTM_ACCOUNT_SECRET_KEY, WEBSITE, INDUSTRY_TYPE_ID, TRANSACTION_URL, TRANSACTION_STATUS_URL } = require('./config'),
	paytm_checksum = require('./paytm/checksum'),
	{ calculateTotalAmount, UNPAID } = require('./utils');

const express = require('express'),
	querystring = require('querystring'),
	request = require('request'),
	uuidv4 = require('uuid/v4'),
	pug = require('pug'),
	moment = require('moment'),
	sessions = require("client-sessions");

const app = express();

app.set('view engine', 'pug')
const router = express.Router();
const transaction_slip = pug.compileFile('slip.pug');


router.post('/payment_status', async (req, res) => {
	if (!req.body) {
		res.sendStatus(400);
		return
	}
	let { ORDERID,
		MID,
		TXNID,
		TXNAMOUNT,
		PAYMENTMODE,
		CURRENCY,
		TXNDATE,
		STATUS,
		RESPCODE,
		RESPMSG,
		GATEWAYNAME,
		BANKTXNID,
		BANKNAME,
		CHECKSUMHASH } = req.body;

	let params = {
		'MID': MID,
		'ORDERID': ORDERID,
		'CHECKSUMHASH': CHECKSUMHASH
	};

	// save object into corressponding record
	let db = req.app.locals.mongodb;

	let record = await db.collection('guests').findOneAndUpdate({
		"order_id": ORDERID,
	}, {
			$set: { "tx_details": req.body, "status": STATUS },
		}, { returnNewDocument: true });
	let CUST_ID = record.value.customer_id;
	let ZION_ID = record.value._id;
	console.log("record", JSON.stringify(record), "customerid", CUST_ID, "ZION_ID", ZION_ID)

	let paramarray = {
		"MID": PAYTM_MID,
		"ORDERID": ORDERID
	}
	paytm_checksum.genchecksum(paramarray, PAYTM_ACCOUNT_SECRET_KEY, function (err, response) {
		if (err) {
			res.send(transaction_slip({
				ZION_ID: "NONE",
				ORDER_ID: ORDERID,
				TXN_AMOUNT: TXNAMOUNT,
				TXN_ID: TXNID,
				CUST_ID: CUST_ID,
				TXN_STATUS: "FAILED TO VERIFY",
				TXN_DATE: TXNDATE
			}));
		}
		let postData = `JsonData=${JSON.stringify(response)}`
		console.log(postData)
		request.post({ url: TRANSACTION_STATUS_URL, headers: { "Content-Type": "application/json" }, body: postData }, (err, response, body) => {
			if (err || response.statusCode != 200) {
				console.error(TRANSACTION_STATUS_URL, response.statusCode, err)
				res.send(transaction_slip({
					ZION_ID: "NONE",
					ORDER_ID: ORDERID,
					TXN_AMOUNT: TXNAMOUNT,
					TXN_ID: TXNID,
					CUST_ID: CUST_ID,
					TXN_STATUS: "FAILED TO VERIFY",
					TXN_DATE: TXNDATE
				}));
				return
			}
			body = JSON.parse(body)
			console.log("BODY", body)
			if (body.RESPCODE == "01") {
				// Render a set of data
				res.send(transaction_slip({
					ZION_ID: ZION_ID,
					ORDER_ID: ORDERID,
					TXN_AMT: TXNAMOUNT,
					TXN_ID: TXNID,
					CUST_ID: CUST_ID,
					TXN_STATUS: STATUS,
					TXN_DATE: TXNDATE
				}))
			} else {
				res.send(transaction_slip({
					ZION_ID: "NONE",
					ORDER_ID: ORDERID,
					TXN_AMT: TXNAMOUNT,
					TXN_ID: TXNID,
					CUST_ID: CUST_ID,
					TXN_STATUS: STATUS,
					TXN_DATE: TXNDATE
				}))
			}
		})
	});
})

router.post("/submit", async (req, res, next) => {
	console.log(req.body);
	if (!req.body) {
		res.sendStatus(400)
		return next();
	}

	if (!(req.body.name &&
		req.body.dob &&
		req.body.contact_no &&
		req.body.emailid &&
		req.body.college_name &&
		req.body.gender &&
		req.body.payment_method && (req.body.payment_method.toLowerCase().trim() == "cash" || req.body.payment_method.toLowerCase().trim() == "online") &&
		req.body.college_id)) {
		res.sendStatus(400);
		return next();
	}

	let { name, dob, payment_method, contact_no, gender, emailid, college_id, college_name, accommodation } = req.body;

	let mongodb = req.app.locals.mongodb;
	let uuid = uuidv4();
	if (accommodation && accommodation.toLowerCase() === 'on') {
		accommodation = true;
	} else {
		accommodation = false;
	}
	let totalAmount = calculateTotalAmount(accommodation, college_name);
	if (college_name[0].toLowerCase().trim() == "coer") {
		college_name = college_name[0];
	} else {
		college_name = college_name[1];
	}

	let record = await mongodb.collection('guests').findOne({
		"customer_id": emailid,
		"status": {
			"$in": ["TXN_SUCCESS", "PENDING", "CASH/UNPAID"]
		}
	})
	console.log("find record", record)
	if (record) {
		if (record.status.toLowerCase() == "pending") {
			res.send(`You have already registered. Your status is currently pending. Please try after some time meanwhile You can check your transaction status at https://register.coerzion.co.in/user_status`)
		} else if (record.status.toLowerCase() == "cash/unpaid") {
			res.send(`You have already registered. You chose to pay with Cash. Go to ZION Helpdesk in COER to pay and collect your pass. Your Zion Id is ${record._id}`)
		} else if (record.status.toLowerCase() == "cash/paid") {
			res.send(`You have already registered. You paid with Cash. Go to ZION Helpdesk in COER to collect your pass. Your Zion Id is ${record._id}`)
		} else {
			res.send(`You have already registered with Zion ID=${record._id}`)
		}
		return
	}
	let insertKey = await getNextSequence("userid", req.app.locals.mongodb);

	let time = moment().utcOffset(330).toString();

	if (payment_method.toLowerCase() == "cash") {
		mongodb.collection("guests").insertOne({
			"_id": insertKey,
			"customer_id": emailid,
			"name": name,
			"dob": dob,
			"payment_method": payment_method.toUpperCase(),
			"order_id": uuid,
			"contact_no": contact_no,
			"accommodation": accommodation,
			"status": "CASH/UNPAID",
			"amount": totalAmount,
			"gender": gender.toLowerCase(),
			"college_name": college_name,
			"college_id": college_id,
			"user-agent": req.headers['user-agent'],
			"time": time,
			"registration_card_generated": false,
		})
		console.log(`[INFO] Cash transaction for ${totalAmount} with ZionID ${insertKey}, orderid ${uuid} and customer id ${emailid} created at ${time}`);
		// Render a set of data
		res.send(transaction_slip({
			ZION_ID: insertKey,
			ORDER_ID: uuid,
			TXN_AMT: totalAmount,
			TXN_ID: "NONE",
			CUST_ID: emailid,
			TXN_STATUS: "CASH/UNPAID",
			TXN_DATE: time,
		}));
		return next();
	} else {
		mongodb.collection("guests").insertOne({
			"_id": insertKey,
			"customer_id": emailid,
			"name": name,
			"dob": dob,
			"payment_method": payment_method.toUpperCase(),
			"order_id": uuid,
			"contact_no": contact_no,
			"accommodation": accommodation,
			"status": UNPAID,
			"amount": totalAmount,
			"gender": gender.toLowerCase(),
			"college_name": college_name,
			"time": time,
			"college_id": college_id,
			"user-agent": req.headers['user-agent'],
			"registration_card_generated": false,
		})
	}


	let paramarray = {};
	paramarray['MID'] = PAYTM_MID; //Provided by Paytm
	paramarray['ORDER_ID'] = uuid; //unique OrderId for every request
	paramarray['CUST_ID'] = emailid;  // unique customer identifier
	paramarray['INDUSTRY_TYPE_ID'] = INDUSTRY_TYPE_ID; //Provided by Paytm
	paramarray['CHANNEL_ID'] = 'WEB'; //Provided by Paytm
	paramarray['TXN_AMOUNT'] = totalAmount.toString(); //transaction amount
	paramarray['WEBSITE'] = WEBSITE; //Provided by Paytm
	paramarray['CALLBACK_URL'] = `https://register.coerzion.co.in/backend/payment_status`;//Provided by Paytm
	paramarray['EMAIL'] = emailid;// customer email id
	paramarray['MOBILE_NO'] = contact_no;// customer 10 digit mobile no.

	console.log("paramarray", paramarray);
	paytm_checksum.genchecksum(paramarray, PAYTM_ACCOUNT_SECRET_KEY, function (err, response) {

		res.write("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n");
		res.write("<html>\n");
		res.write("<head>\n");
		res.write("<title>Merchant Checkout Page</title>\n");
		res.write("</head>\n");
		res.write("<body>\n");
		res.write("<center><h1>Please do not refresh this page...</h1></center>\n");
		res.write("<form method='post' action='" + TRANSACTION_URL + "' name='f1'>\n");
		for (let name in paramarray) {
			if (name === "CHECKSUMHASH") {
				continue;
			}
			res.write("<input type='hidden' name='" + name + "' value='" + paramarray[name] + "'>\n");
		}
		res.write("<input type='hidden' name='CHECKSUMHASH' value='" + response["CHECKSUMHASH"] + "'>\n");

		res.write("</form>\n");
		res.write("<script type='text/javascript'>\n");
		res.write("document.f1.submit();\n");
		res.write("</script>\n");
		res.write("</body>\n");
		res.write("</html>\n");
		res.end();
	});
})

async function getNextSequence(name, db) {
	var ret = await db.collection('counter').findOneAndUpdate(
		{ _id: name },
		{
			$inc: { seq: 1 },
		},
		{
			returnNewDocument: true,
			upsert: true,
			projection: { seq: 1 }
		}
	);

	let seq = 0;
	if (ret.value && ret.value.seq) {
		seq = ret.value.seq;
	}
	return "ZION_" + seq;
}

router.post("/login", (req, res, next) => {
	console.log(req.body)
	if (!req.body || (!req.body.query_id || !req.body.password)) {
		res.sendStatus(400)
		return;
	}
	if (req.body.query_id === "zadmin" && req.body.password === "zion@admin19") {
		req.session.zid = "zadmin"
		req.session.pwd = "zion@admin19"
		res.redirect('/query')
	} else {
		res.sendStatus(403)
	}
})


router.get("/query_zionid", async (req, res, next) => {
	console.log(req.query);

	if (!(req.query && req.query.zion_id)) {
		res.sendStatus(400)
		return next();
	}

	let { zion_id } = req.query;
	if (req.session && req.session.zid === "zadmin" && req.session.pwd === "zion@admin19") {
		let mongodb = req.app.locals.mongodb;
		let record = await mongodb.collection('guests').findOne({
			"_id": zion_id.toUpperCase(),
		})
		console.log("find record", record)
		if (record) {
			res.json(record)
		} else {
			res.json({
				id: 'NONE',
				customer_id: 'NONE',
				name: 'NONE',
				dob: 'NONE',
				payment_method: 'NONE',
				contact_no: 'NONE',
				accommodation: 'NONE',
				status: 'NONE',
				amount: 'NONE',
				gender: 'NONE',
				college_name: 'NONE',
				college_id: 'NONE'
			})
		}
	} else {
		console.log("Bad Request")
		res.sendStatus(403)
	}
})

router.post("/user_update", async (req, res, next) => {
	console.log(req.body)
	if (req.session && req.session.zid != "zadmin" || req.session.pwd != "zion@admin19") {
		res.sendStatus(403);
		return next();
	}

	if (!req.body) {
		res.sendStatus(400)
		return;
	}

	if (!(req.body.zion_id &&
		req.body.payment_status &&
		req.body.zion_pass_generated)) {
		res.sendStatus(400);
		return next();
	}

	let { zion_id, name, college_id, payment_status, zion_pass_generated } = req.body;

	if (!(payment_status.toUpperCase() == "CASH/UNPAID" || payment_status.toUpperCase() == "CASH/PAID")) {
		res.sendStatus(400);
		return next();
	}

	let db = req.app.locals.mongodb;
	let update_query = {
		$set: { "registration_card_generated": zion_pass_generated },
	};

	if (name) {
		update_query["$set"].name = name;
	}
	if (college_id) {
		update_query["$set"].college_id = college_id;
	}

	let rec = await db.collection('guests').findOne({
		"_id": zion_id
	})

	if (rec['status'] === 'CASH/UNPAID') {
		update_query["$set"]["status"] = payment_status;
	}
	let record = await db.collection('guests').findOneAndUpdate({
		"_id": zion_id,
	}, update_query, { returnNewDocument: true })
	res.json(record)
})

router.post("/user_payment_status", async (req, res, next) => {
	if (!(req.body || req.body.query_type || req.body.query_id || req.body.password)) {
		console.log(req.body)
		res.sendStatus(400)
		return
	}
	let { query_type, query_id, password } = req.body
	let mongodb = req.app.locals.mongodb;
	if (query_type.trim() === "ZION_ID") {
		let record = await mongodb.collection('guests').findOne({
			"_id": query_id.trim().toUpperCase()
		})
		console.log("query_record", record)
		if (record) {
			if (password === record.college_id) {
				let tx_details = "NONE"
				if (record["tx_details"]) {
					tx_details = record["tx_details"]["TXNID"]
				}
				res.send(transaction_slip({
					ZION_ID: record["_id"],
					ORDER_ID: record["order_id"],
					NAME: record["name"],
					TXN_AMT: record["amount"],
					TXN_ID: tx_details,
					CUST_ID: record["customer_id"],
					TXN_STATUS: record["status"],
					TXN_DATE: record["time"],
				}));
			} else {
				res.send("OOOOPS you have entered wrong password...")
			}
		} else {
			res.send("It seems you haven't registered yourself yet...")
			console.log("zion id doesn't exist")
		}
	} else {
		let record = await mongodb.collection('guests').findOne({
			"tx_details.TXNID": query_id.trim()
		})
		console.log("transaction_details", record)
		if (record) {
			let paramarray = {
				"MID": record["tx_details"]["MID"],
				"ORDERID": record["tx_details"]["ORDERID"]
			}
			paytm_checksum.genchecksum(paramarray, PAYTM_ACCOUNT_SECRET_KEY, function (err, response) {
				if (err) {
					res.send(transaction_slip({
						ZION_ID: "NONE",
						ORDER_ID: record["tx_details"]["ORDERID"],
						TXN_AMOUNT: record["tx_details"]["TXNAMOUNT"],
						NAME: record["name"],
						TXN_ID: record["tx_details"]["TXNID"],
						CUST_ID: record["customer_id"],
						TXN_STATUS: "FAILED TO VERIFY",
						TXN_DATE: record["tx_details"]["TXNDATE"]
					}));
				}
				let postData = `JsonData=${JSON.stringify(response)}`
				console.log(postData)
				request.post({ url: TRANSACTION_STATUS_URL, headers: { "Content-Type": "application/json" }, body: postData }, async (err, response, body) => {
					if (err || response.statusCode != 200) {
						console.error(TRANSACTION_STATUS_URL, response.statusCode, err)
						res.send(transaction_slip({
							ZION_ID: "NONE",
							ORDER_ID: record["tx_details"]["ORDERID"],
							TXN_AMOUNT: record["tx_details"]["TXNAMOUNT"],
							NAME: record["name"],
							TXN_ID: record["tx_details"]["TXNID"],
							CUST_ID: record["customer_id"],
							TXN_STATUS: "FAILED TO VERIFY",
							TXN_DATE: record["tx_details"]["TXNDATE"]
						}));
						return
					}
					body = JSON.parse(body)
					console.log("BODY", body)
					if (body.RESPCODE == "01") {
						if (record["status"] === "PENDING") {
							let rec = await mongodb.collection('guests').findOneAndUpdate({
								"tx_details.TXNID": query_id.trim(),
							}, { $set: { "status": body.STATUS } },
								{ returnNewDocument: true }
							)
						}
						// Render a set of data
						res.send(transaction_slip({
							ZION_ID: record["_id"],
							ORDER_ID: body.ORDERID,
							TXN_AMT: body.TXNAMOUNT,
							NAME: record["name"],
							TXN_ID: body.TXNID,
							CUST_ID: record["customer_id"],
							TXN_STATUS: body.STATUS,
							TXN_DATE: body.TXNDATE
						}))
					} else {
						res.send(transaction_slip({
							ZION_ID: record["_id"],
							ORDER_ID: body.ORDERID,
							TXN_AMT: body.TXNAMOUNT,
							NAME: record["name"],
							TXN_ID: body.TXNID,
							CUST_ID: record["customer_id"],
							TXN_STATUS: body.STATUS,
							TXN_DATE: body.TXNDATE
						}))
					}
				})
			});
		} else {
			res.send("Wrong Transaction Id")
		}

	}
})


module.exports = router;
