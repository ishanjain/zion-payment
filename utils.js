module.exports = {
    calculateTotalAmount: (accomodation, college_name) => {
        let amount = 0;

        if (college_name instanceof Array) {
            if (college_name[0].toLowerCase().trim() === "coer") {
                college_name = college_name[0].toLowerCase().trim()
            } else {
                college_name = college_name[1].toLowerCase().trim();
            }
        }

        if (college_name == "coer") {
            amount += 250;
        } else {
            amount += 400;
        }
        if (accomodation) {
            amount += 100
        }

        return amount;
    },

    // User Status
    "TXN_SUCCESS": "TXN_SUCCESS",
    "TXN_FAILURE": "TXN_FAILURE",
    "PENDING": "PENDING",
    "UNPAID": "UNPAID"
};


