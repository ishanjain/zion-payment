require('dotenv').config();

const express = require('express'),
    mongodbClient = require("mongodb").MongoClient,
    bodyParser = require("body-parser"),
    minify = require('express-minify'),
    sessions = require("client-sessions"),
    { MONGODB_URL } = require('./config'),
    router = require('./router');
const app = express();

app.set('view engine', 'pug')
app.use(minify());
app.use("/backend", bodyParser.urlencoded({ extended: true }));
app.use("/backend", bodyParser.json());
app.use(sessions({
    cookieName: 'session', // cookie name dictates the key name added to the request object
    secret: 'morgulis@09', // should be a large unguessable string
    duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms
    activeDuration: 1000 * 60 * 5, // if expiresIn < activeDuration, the session will be extended by activeDuration milliseconds
}));


app.use('/', express.static("public"))
app.use('/admin_login', express.static("public/admin_login.html"))
app.use('/user_status', express.static("public/status.html"))


mongodbClient.connect(MONGODB_URL, { useNewUrlParser: true }, (err, client) => {
    if (err) {
        console.error(`[ERROR] ${err}`)
        process.exit(1);
    }

    let db = client.db("zion");


    app.locals.mongodb = db;
})

app.use("/backend", router);


app.get('/query', (req, res, next) => {
	if (req.session && req.session.zid != "zadmin" || req.session.pwd != "zion@admin19") {
        res.redirect("/admin_login")
        return
    }
    res.sendFile(__dirname + "/public/query.html");

})

app.listen(process.env.PORT || 5000, (err) => {
    console.log(`[INFO] Server started on ${process.env.PORT || 5000}`)
});

