module.exports = {
    // PayTM Merchant ID 
    "PAYTM_MID": process.env.PAYTM_MID || "sNVfgz44505506321135",
    // PayTM Account Secret Key
    "PAYTM_ACCOUNT_SECRET_KEY": process.env.PAYTM_ACCOUNT_SECRET_KEY || "Uu2di4M&E83S%76A",

    "MONGODB_URL": process.env.MONGODB_URL || "mongodb://localhost:27017/zion",

    "WEBSITE": process.env.PRODUCTION ? process.env.WEBSITE : "WEBSTAGING",

    "INDUSTRY_TYPE_ID": process.env.PRODUCTION ? process.env.INDUSTRY_TYPE_ID : "Retail",

    "TRANSACTION_URL": process.env.PRODUCTION ? "https://securegw.paytm.in/theia/processTransaction" : "https://securegw-stage.paytm.in/theia/processTransaction",
    "TRANSACTION_STATUS_URL": process.env.PRODUCTION ? "https://securegw.paytm.in/merchant-status/getTxnStatus" : "https://securegw-stage.paytm.in/merchant-status/getTxnStatus"
}